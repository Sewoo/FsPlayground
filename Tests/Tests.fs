namespace Playground
open Xunit
open Playground.MyMath
open System.Collections.Generic

module Tests =
    [<Fact>]
    let Fibonacci() =
        let actual = 
            [1..8] |> List.map fibonacci
        Assert.True(seq [1;1;2;3;5;8;13;21] = seq actual)

    [<Fact>]
    let Factorial() =
        let actual = 
            [1..6] |> List.map factorial
        Assert.True(seq [1;2;6;24;120;720] = seq actual)
    
    [<Fact>]
    let Palidrone() =
        Assert.True(Strings.palindrome("ada ada"))
        Assert.True(Strings.palindrome([1;2;1]))
        Assert.False(Strings.palindrome("qweasd"))
        Assert.False(Strings.palindrome([1;2;3]))

    [<Fact>]
    let Linear() = 
        Assert.Equal(-0.5m, linear 2m 1m)
        Assert.Equal(0.5m, linear 2m -1m)
        Assert.Equal(-1m, linear 1m 1m)

    [<Fact>]
    let Quicksort() =
        let rand = new System.Random(System.DateTime.Now.Millisecond)
        let unsorted = List.init 100 (fun x-> rand.Next())
        let sorted = quicksort unsorted
        Assert.True(List.sort unsorted = sorted)

    [<Fact>]
    let ComplexAdd() =
        let a = Complex(1.0, 2.0)
        let b = Complex(2.2, 3.3)
        Assert.Equal(Complex(3.2, 5.3), a + b)
        Assert.NotEqual(Complex(0.0, 5.3), a + b)

    [<Fact>]
    let ComplexMultiple() =
        let a = Complex(1.0, 1.0)
        let b = Complex(1.0, 1.0)
        Assert.Equal(Complex(0.0, 2.0), a * b)

    [<Fact>]
    let fft() =
        let after = Array.create 4 (Complex(1.,0.)) |> fft1
        Assert.Equal(4.0, after.[0].Real)
        Assert.Equal(0.0, after.[0].Imag)

        Assert.Equal(0.0, after.[1].Real)
        Assert.Equal(0.0, after.[1].Imag)

        Assert.Equal(0.0, after.[2].Real)
        Assert.Equal(0.0, after.[2].Imag)

        Assert.Equal(0.0, after.[3].Real)
        Assert.Equal(0.0, after.[3].Imag)