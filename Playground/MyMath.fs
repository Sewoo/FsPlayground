﻿namespace Playground
open System
open System.Collections.Generic

module MyMath =
    let rec fibonacci n =
        match n with
        |1|2 -> 1
        |x -> fibonacci(x-1) + fibonacci(x-2)

    let rec factorial n =
        match n with
        |1|2 -> n
        |x -> x * factorial(x-1)

    let linear (a:decimal) (b:decimal) = -b/a

    let rec quicksort collection =
        match collection with
        |[] -> []
        |head::tail ->
            let smaller =
                tail 
                |> List.filter (fun e -> e < head)
                |> quicksort
            let larger =
                tail
                |> List.filter (fun e -> e >= head)
                |> quicksort
            List.concat [smaller; [head]; larger]

    type Complex(real, imag) =
        member this.Real = real
        member this.Imag = imag
        static member FromPolar(r, radians) =
            Complex(r * Math.Cos(radians), r * Math.Sin(radians))

        static member (+) (a:Complex, b:Complex) =
            Complex(a.Real + b.Real, a.Imag + b.Imag)

        static member (-) (a:Complex, b:Complex) =
            Complex(a.Real - b.Real, a.Imag - b.Imag)
        static member (*) (a:Complex, b:Complex) =
            Complex(((a.Real*b.Real) - (a.Imag*b.Imag), (a.Real*b.Imag + (a.Imag*b.Real))))

        override this.Equals(ob) =
            let b = ob :?> Complex
            this.Real = b.Real && this.Imag = b.Imag

        override this.ToString() =
            this.Real.ToString() + ", " + this.Imag.ToString() + "i"
        override this.GetHashCode() =
            this.Real.GetHashCode() * 17 + this.Imag.GetHashCode() * 22

    let dft (input:Complex list) =
        let inLength = input.Length
        let output = new List<Complex>()
        let mutable k = 0
        while k < inLength do
            output.Add(Complex(0.0, 0.0))
            let mutable n = 0
            while n < inLength do
                output.[k] <- output.[k] + Complex.FromPolar(1.0, -2.0*Math.PI*(float n)*(float k)/(float inLength)) * input.[n];
                n <- n + 1
            k <- k + 1
        output
    
    let rec fft1 (zs: _ array) : _ array =
        let n = zs.Length
        let mutable result = Array.create n (Complex(0.,0.))
        if n>1 then
            let even = Array.init (n/2) (fun i -> zs.[2*i]) |> fft1
            let odd = Array.init (n/2) (fun i -> zs.[2*i + 1]) |> fft1
            let s = -2.0 * System.Math.PI / float n
            for k=0 to n/2 - 1 do
                let z1, z2 = even.[k], odd.[k] * Complex(cos (s * float k), sin (s * float k))
                result.[k] <- z1 + z2
                result.[k + n/2] <- z1 - z2
        else result <- zs
        result