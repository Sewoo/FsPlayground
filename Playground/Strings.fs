namespace Playground

module Strings =
    let palindrome se =
        let list = Seq.toList(se)
        list = List.rev(list)
